<br><br>
<center><div id="map" style="width:75%;height:400px;"></div></center>
<br><br>

<script>

  function pop() {


    var uluru = {lat: {{$post->lat}}, lng: {{$post->lon}} };

    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 17, center: uluru});

    
    var marker = new google.maps.Marker({position: uluru, map: map, draggable:true});

    marker.addListener('drag', function() {
        
        var x = marker.getPosition().lat();

        var y = marker.getPosition().lng();

        var c = document.getElementById('lat');

        c.value = x;

        c = document.getElementById('lon');

        c.value = y;

    });

    }
   

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSAE9mWkUYiOeE4KDy7rhS3V9ir0tMmgw&callback=pop">
</script>

<br>


<center><h3>
    Drag the marker to position your lab's location.    
</h></center>

@extends('layouts.app')

@section('content')
    
    <h1>Edit Post</h1>
    
    {!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    
        <div class="form-group">
        
            {{Form::label('title', 'Title')}}
            {{Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'Title'])}}

        </div>
    
        <div class="form-group">
            
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', $post->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}

        </div>

        <div class="form-group">
            
            {{Form::label('lat', 'Latitude')}}
            {{Form::text('lat', $post->lon, ['id' => 'lat', 'class' => 'form-control', 'placeholder' => 'Latitude. You can also drag the marker.'])}}

        </div>

        <div class="form-group">
            
            {{Form::label('lon', 'Longitude')}}
            {{Form::text('lon', $post->lon, ['id' => 'lon', 'class' => 'form-control', 'placeholder' => 'Longitude. You can also drag the marker.'])}}

        </div>
    
        <div class="form-group">
            
            {{Form::file('cover_image')}}

        </div>

        
    
        {{Form::hidden('_method', 'PUT')}}
        {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    
    {!! Form::close() !!}

@endsection