<br><br>
<center><div id="map" style="width:75%;height:400px;"></div></center>
<br><br>

<script>

  function pop() {

    var request = new XMLHttpRequest();

    var x = 'asd';

    var cog = '';

    request.open('GET', 'https://api.ipify.org?format=json', true)

    request.onload = function() {
      // Begin accessing JSON data here
      var data = JSON.parse(this.response)

      if (request.status >= 200 && request.status < 400) {              

        x = data.ip;    
      
        var blob = "";

        blob = blob.concat('http://api.ipstack.com/', x,'?access_key=76e7e1f2e7c8e8f2070c3754672667a4');                  

        console.log(blob);

        var rq =  new XMLHttpRequest();
        
        rq.open('GET', blob, true);

        rq.onload = function() {

          var peta = JSON.parse(this.response);

          console.log(peta.latitude);

          console.log(peta.longitude);

          var uluru = {lng: peta.longitude, lat: peta.latitude};

          console.log(uluru);

          var map = new google.maps.Map(
          
            document.getElementById('map'), {zoom: 17, center: uluru}
            
          );          

          var marker = new google.maps.Marker({position: uluru, map: map, draggable:true});

          var c = document.getElementById('lat');

          c.value = peta.latitude;

          c = document.getElementById('lon');

          c.value = peta.longitude;

          marker.addListener('drag', function() {
          
            var x = marker.getPosition().lat();

            var y = marker.getPosition().lng();

            var c = document.getElementById('lat');

            c.value = x;

            c = document.getElementById('lon');

            c.value = y;

          });

        }

        rq.send();        

      }

      else {
      
        console.log('error')

      }

    }

    request.send();
    
    
    
  }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSAE9mWkUYiOeE4KDy7rhS3V9ir0tMmgw&callback=pop">
</script>

<br>


<center><h3>
    Drag the marker to position your lab's location.    
</h></center>

@extends('layouts.app')



@section('content')
    
    <h1>Create Post</h1>
    
    {!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    
        <div class="form-group">
        
            {{Form::label('title', 'Title')}}
            {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}

        </div>
    
        <div class="form-group">
            
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}

        </div>

        <div class="form-group">
            
            {{Form::label('lat', 'Latitude')}}
            {{Form::text('lat', '', ['id' => 'lat', 'class' => 'form-control', 'placeholder' => 'Latitude. You can also drag the marker.'])}}

        </div>

        <div class="form-group">
            
            {{Form::label('lon', 'Longitude')}}
            {{Form::text('lon', '', ['id' => 'lon', 'class' => 'form-control', 'placeholder' => 'Longitude. You can also drag the marker.'])}}

        </div>
    
        <div class="form-group">
            
            {{Form::file('cover_image')}}

        </div>
    
        {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    
    {!! Form::close() !!}

@endsection