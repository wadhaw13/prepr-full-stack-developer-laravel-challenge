@extends('layouts.app')
<div id="map" style="width:100%;height:400px;"></div>

<script>
    
    function myMap() {
    
        
        var uluru = {lat: {{$posts->lat}}, lng: {{$posts->lon}} };
        
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 17, center: uluru});
        
        var latLng = new google.maps.LatLng("43","23");
        var marker = new google.maps.Marker({position: uluru, map: map});

    }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSAE9mWkUYiOeE4KDy7rhS3V9ir0tMmgw&callback=myMap"></script>




@section('content')
    
    <a href="/posts" class="btn btn-primary">Go Back</a>

    <hr>

    <h1>{{$posts->title}}</h1>    
   
    Notes: {!!$posts->body!!}    
   
    <br>

    Latitide: {!!$posts->lat!!} Longitude: {!!$posts->lon!!}   
    
    <br>    

    <a href="https://www.google.com/maps/search/?api=1&query={{$posts->lat}},{{$posts->lon}}">See on Google Maps</a>

    <br>

    <small>Written on {{$posts->created_at}} by {{$posts->user->name}}</small>

    @if(!Auth::guest())

        @if (Auth::user()->id == $posts->user_id)

            <hr>
        
            <a href = "/posts/{{$posts->id}}/edit" class = "btn btn-primary">Edit this lab.</a>
        
            <br><br>

            {!!Form::open(['action' => ['PostsController@destroy', $posts->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
    
        @endif

    @endif

@endsection



