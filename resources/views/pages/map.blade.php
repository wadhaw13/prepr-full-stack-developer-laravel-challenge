@extends('layouts.app')


<script>
    
    function myMap() {
            
        var app = <?php echo json_encode($serviceArray); ?>;           
        
        console.log();

        if (Object.keys(app).length === 0) {

            var sss = {lat: 3, lng: 1 };

            var xxx = new google.maps.Map(
            document.getElementById('map'), {zoom: 4, center: sss});

            var text = document.getElementById('popper');

            text.innerHTML = "No labs found.";

            return;
        }

        var uluru = {lat: app[0].lat, lng: app[0].lon };
        
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 3, center: uluru});                            

        var chul = uluru;

        var curr = new google.maps.Marker({position: chul, map: map, idx: 0, title: 'asd'})

        for (i in app){

            //console.log(app[i].lat, ' ', app[i].lon);

            var latLng = new google.maps.LatLng(app[i].lat,app[i].lon);

            var marker = new google.maps.Marker({position: latLng, map: map, idx: app[i].id, title: app[i].title});               

            marker.addListener('click', function() {
          
                var text = document.getElementById('popper');

                text.innerHTML = '<a href=/posts/'.concat(this.idx).concat('>').concat(this.title).concat('</a>');               

                curr.setAnimation(null);

                this.setAnimation(google.maps.Animation.BOUNCE);

                curr = this;

            });                

        }

    }

</script>

<div id="map" style="width:100%;height:600px;"></div>
<br>
<center><div id="popper">Click on a marker for more details.</div></center>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSAE9mWkUYiOeE4KDy7rhS3V9ir0tMmgw&callback=myMap"></script>

