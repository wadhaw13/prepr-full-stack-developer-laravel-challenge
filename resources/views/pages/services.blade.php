@extends('layouts.app')
            
@section('content')

    <h1> {{$title}} </h1>

    @if (count($serviceArray) > 0)

        <ul class ="list-group" >

            @foreach ($serviceArray as $item)
            
                <li class = "list-group-item">{{$item}}</li>
                <br>

            @endforeach
    
        </ul>                         

    @endif

@endsection
