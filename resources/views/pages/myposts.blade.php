@extends('layouts.app')
            
@section('content')

    @guest

    <center>
        
        <div class="jumbotron">
            
            Please Log In.

        </div>

    </center>

    @else

    <div class="jumbotron">
        <center>
            <h1>Your Posts</h1>
        </center>
    
    </div>

    <table class = "table table-striped">
    
        <tr>

            <th>Title</th>
            <th></th>
            <th></th>
        </tr>
    
        @foreach ($posts as $post)
            
        <tr>

            <td>{{$post->title}}</td>
            <td><a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a></td>
            <td>{!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                {!!Form::close()!!}
            </td>
        </tr>

        @endforeach

    </table>

    @endguest

@endsection


