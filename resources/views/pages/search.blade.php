@extends('layouts.app')
            
@section('content')    

    @if ($data != null)

        <table class = "table">
        
            @foreach ($data as $post)
                
            <tr>

                <td>{{$post->title}}</td>
                <td>{{$post->body}}</td>
                <td><a href="/posts/{{$post->id}}/" class="btn btn-primary">View</a></td>

            </tr>

            @endforeach

        </table>

    @else 
    
        <div class = "container">

            <center>

                <div class = "jumbotron">

                    <h1> No results found </h1>                    

                </div>

            </center>            

        </div>

    @endif

    <a href="/" class="btn btn-primary">Go Back</a>

@endsection