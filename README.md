# Prepr Full Stack Developer Summer Work Challenge

A simple CRUD application made for the  <a href = "https://preprlabs.org/challengeManager/full-stack-developer-summer-work-challenge"> Prepr Summer Work Challenge </a> using the PHP Laravel framework.

# Overview

In this project I satisfy the following project requirements:

* Allow a user to login and logout <br>
* CRUD functionality (Create / Read / Update / Delete) for Labs<br>
* Labs contain the following information: Name, date added, location<br>
* Show a list of available labs<br>
* Users can click the labs and click to open up a google maps query, showing the location of the lab<br>
* Contain a maps page that drops a pin for each of the lab locations<br>

# Extra functionality 

*  Add locations by picking them on the map.
*  Search for labs by their name and title.

# Resources

**Invaluable help provided by Travery Media's Laravel From Scratch course:** [Laravel From Scratch](https://www.youtube.com/playlist?list=PLillGF-RfqbYhQsN5WMXy6VsDMKGadrJ-)

Multiple dependecies are required to run this project which include NPM, Composer for PHP, Artisan for PHP and of course the Laravel Framework.

XAMPP is used for setting up the server and the database.

# Demonstration Video

[On YouTube](https://www.youtube.com/watch?v=gASL0OCxhbE)

# Images

![1](/resources/mdimages/1.png)
![2](/resources/mdimages/2.png)
![3](/resources/mdimages/3.png)
![4](/resources/mdimages/4.png)
![5](/resources/mdimages/5.png)
![6](/resources/mdimages/6.png)
![7](/resources/mdimages/7.png)
![8](/resources/mdimages/8.png)
![9](/resources/mdimages/9.png)





