<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;

use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    public function index() {

        $posts = Post::all()->reverse()->take(5);
      
        $all = Post::all();

        return view('pages.index')->with('posts', $posts, 'all', $all);
        

    }

    public function about() {

        return view('pages.about');

    }

    public function services() {

        $data = array (

            'title' => 'Services',
            'serviceArray' => ['Create', 'Read', 'Update', 'Delete'],
            'pop' => 'pops'

        );

        return view('pages.services')->with($data);

    }

    public function map()  {

        $data = Post::all()->reverse()->take(5);

        return view('pages.map')->with('serviceArray',$data);

    }

    public function search(Request $req)  {

        $q = $req->input('search');

        $data = DB::table('posts')->where('title','LIKE','%'.$q.'%')->orWhere('body','LIKE','%'.$q.'%')->get();

        if ($data == '[]') return view('pages.search')->with('data', null);;

        return  view('pages.search')->with('data', $data);

    }

}
