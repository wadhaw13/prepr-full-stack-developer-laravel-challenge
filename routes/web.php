<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('/about', 'PagesController@about');

Route::get('/services', 'PagesController@services');

Route::get('/map', 'PagesController@map');

Route::get('/search', function(Request $req) {

    $q = $req->input('search');

    $data = DB::table('posts')->where('title','LIKE','%'.$q.'%')->orWhere('body','LIKE','%'.$q.'%')->get();

    if ($data == '[]') return view('pages.search')->with('data', null);;

    return  view('pages.search')->with('data', $data);

});

Route::resource('posts', 'PostsController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
